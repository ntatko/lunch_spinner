import React, { Component } from 'react';
import {
  Card,
  CardPrimaryAction,
  CardActionButton,
  CardActionIcon,
  CardActions,
  CardActionButtons,
  CardActionIcons
} from '@rmwc/card';
import { Typography } from '@rmwc/typography';
import { ListDivider } from '@rmwc/list';
import { Icon } from '@rmwc/icon';
import { IconButton } from '@rmwc/icon-button';

import '@material/card/dist/mdc.card.css';
import '@material/button/dist/mdc.button.css';
import '@material/icon-button/dist/mdc.icon-button.css';
// import '../../../../node_modules/@rmwc/icon/icon.css';

import MobileStepper from '@material-ui/core/MobileStepper';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const pets = [
  {
    "id": 46,
    "name": "Snowy",
    "birthday": "2016-12-25T00:00:00.000Z",
    "color": "Brown",
    "weight": 30,
    "is_spayed_neutered": true,
    "breed": {
      "id": 22,
      "name": "Wire Fox Terrier"
    },
    "vaccinations": [
      {
        "id":1,
        "vaccination_date": "2018-11-30T00:00:00.000Z",
        "serial_number": "190ASD622",
        "type": {
          "id": 2,
          "name": "Bordetella",
          "period": "yearly"
        }
      }
    ],
    "images": [
      {
        "id": 25,
        "name": "scruffy.jpg",
        "description": "a picture of scruffy",
        "url": "https://i.pinimg.com/originals/92/5d/a3/925da3b1ec5b5beaab08a24c5ea4c574.gif",
        "rank": 1,
        "created_by_user_id": 1,
        "created": "2018-11-17T20:46:12.691Z",
        "updated": null
      },
      {
        "id": 26,
        "name": "scruffy.jpg",
        "description": "a picture of scruffy",
        "url": "https://static.comicvine.com/uploads/square_small/11/111746/5104808-milou_seul.jpg",
        "rank": 2,
        "created_by_user_id": 1,
        "created": "2018-11-17T20:46:12.691Z",
        "updated": null
      },
    ],
    "created": "2018-11-17T20:46:12.691Z",
    "updated": null
  },
  {
    "id": 108,
    "name": "Snoopy",
    "birthday": "2016-12-25T00:00:00.000Z",
    "color": "Brown",
    "weight": 30,
    "is_spayed_neutered": true,
    "breed": {
      "id": 8,
      "name": "Beagle"
    },
    "vaccinations": [
      {
        "id":1,
        "vaccination_date": "2018-11-30T00:00:00.000Z",
        "serial_number": "190ASD622",
        "type": {
          "id": 2,
          "name": "Bordetella",
          "period": "yearly"
        }
      }
    ],
    "images": [
      {
        "id": 25,
        "name": "on_the_doghouse",
        "description": "snoopy on the doghouse",
        "url": "https://cdn.theatlantic.com/assets/media/img/2015/09/30/BOB_Boxer_Peanuts_Opener_HP/1920.jpg?1443632690",
        "rank": 2,
        "created_by_user_id": 1,
        "created": "2018-11-17T20:46:12.691Z",
        "updated": null
      },
      {
        "id": 26,
        "name": "snoopy_love",
        "description": "with love, from snoopy",
        "url": "https://www.dhresource.com/0x0s/f2-albu-g5-M00-0F-27-rBVaJFiid_KAWQTEAACJzY-3dS4814.jpg/hot-sale-for-peanuts-snoopy-love-vinyl-funny.jpg",
        "rank": 1,
        "created_by_user_id": 1,
        "created": "2018-11-17T20:46:12.691Z",
        "updated": null
      },
    ],
    "created": "2018-11-17T20:46:12.691Z",
    "updated": null
  },
  {
    "id": 108,
    "name": "Odie",
    "birthday": "2016-12-25T00:00:00.000Z",
    "color": "Brown",
    "weight": 30,
    "is_spayed_neutered": true,
    "breed": {
      "id": 8,
      "name": "Beagle"
    },
    "vaccinations": [],
    "images": [
      {
        "id": 25,
        "name": "glasses",
        "description": "odie with glasses",
        "url": "https://i.pinimg.com/236x/4d/a6/77/4da6771b2f207968e8848c2bbe996385--garfield-pictures-classic-cartoons.jpg",
        "rank": 1,
        "created_by_user_id": 1,
        "created": "2018-11-17T20:46:12.691Z",
        "updated": null
      },
      {
        "id": 26,
        "name": "odie_stick",
        "description": "odie has a stick",
        "url": "https://i.ytimg.com/vi/4VQFvcUSAjg/hqdefault.jpg",
        "rank": 2,
        "created_by_user_id": 1,
        "created": "2018-11-17T20:46:12.691Z",
        "updated": null
      },
    ],
    "created": "2018-11-17T20:46:12.691Z",
    "updated": null
  },
  {
    "id": 108,
    "name": "Scoobie Doo",
    "birthday": "2016-12-25T00:00:00.000Z",
    "color": "Brown",
    "weight": 30,
    "is_spayed_neutered": true,
    "breed": {
      "id": 8,
      "name": "Beagle"
    },
    "vaccinations": [],
    "images": [],
    "created": "2018-11-17T20:46:12.691Z",
    "updated": null
  }
];

class CarouselCard extends Component {

    state = {
        activeStep: 0,
        anchorEl: null
    };

    handleNext = () => {
        this.setState(prevState => ({
            activeStep: prevState.activeStep + 1,
        }));
    };

    handleBack = () => {
        this.setState(prevState => ({
            activeStep: prevState.activeStep - 1,
        }));
    };

    handleStepChange = activeStep => {
        this.setState({ activeStep });
    };

    handleMenuClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = () => {
        this.setState({ anchorEl: null });
    };

    handlePetClick = (petName) => {
        //TODO - add route to allpets#petName
    }

    render() {

        const { activeStep } = this.state;
        const maxSteps = pets.length;
        const theme = {direction: 'ltr'};

        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <Card outlined style={{height: 360, width: '50%', minWidth: 300, maxWidth: '100%'}}>
                <div className='card-header' >
                    <Typography
                        use="menu"
                        tag="div"
                        style={{ padding: '0.5rem 1rem', verticalAlign: 'middle' }}
                        theme="textSecondaryOnBackground"
                        >
                        My Pets
                    </Typography>

                    <div>
                        <IconButton
                          aria-label="More"
                          aria-owns={open ? 'long-menu' : undefined}
                          aria-haspopup="true"
                          onClick={this.handleMenuClick}
                        >
                            <MoreVertIcon />
                        </IconButton>
                        <Menu
                            id="long-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={this.handleMenuClose}
                        >
                            <MenuItem key='pets_menu' onClick={this.handleMenuClose}>
                                Add Pet
                            </MenuItem>
                            <MenuItem key='pets_menu' onClick={this.handleMenuClose}>
                                See All
                            </MenuItem>
                        </Menu>
                    </div>
                </div>

                <ListDivider />
                <div className='pet_profile'>
                    {pets ? (<div className={'pet_profile_slider'}>
                      <AutoPlaySwipeableViews
                          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                          index={activeStep}
                          onChangeIndex={this.handleStepChange}
                          enableMouseEvents
                      >
                          {pets.map((pet, index) => (
                              <CardPrimaryAction onClick={this.handlePetClick(pet.name)}>
                                  <div key={pet.name}>
                                      {Math.abs(activeStep - index) <= 2 ? (
                                          <div style={{textAlign: 'center'}}>
                                              {pet.images[0] ?
                                                  (<img className={'pet_profile'} src={pet.images[0].url} alt={pet.images[0].description} />)
                                                  : (<img className={'pet_profile'} src={'http://petmedmd.com/images/dog-profile.png'} alt={'default image'} />)
                                              }
                                              <div className='caption'>
                                                  <h2 className='caption'>{pets[activeStep].name}</h2>
                                                  {pet.warning ? (
                                                      <Icon onClick={this.handlePetClick(pet.name)}>
                                                          <MoreVertIcon />
                                                      </Icon> )
                                                  : null}
                                              </div>
                                          </div>
                                      ) : null}
                                  </div>
                              </CardPrimaryAction>
                          ))}
                      </AutoPlaySwipeableViews>
                      <MobileStepper
                          steps={maxSteps}
                          position="static"
                          activeStep={activeStep}
                          className={'mobileStepper'}
                          nextButton={
                              <CardActionButton size="small" onClick={this.handleNext} disabled={activeStep === maxSteps - 1} style={{verticalAlign: "middle"}}>
                                  Next
                                  {theme.direction === 'rtl' ? <KeyboardArrowLeft style={{verticalAlign: "middle"}} /> : <KeyboardArrowRight style={{verticalAlign: "middle"}} />}
                              </CardActionButton>
                          }
                          backButton={
                              <CardActionButton size="small" onClick={this.handleBack} disabled={activeStep === 0} style={{verticalAlign: "middle"}}>
                                  {theme.direction === 'rtl' ? <KeyboardArrowRight style={{verticalAlign: "middle"}} /> : <KeyboardArrowLeft style={{verticalAlign: "middle"}} />}
                                  Back
                              </CardActionButton>
                          }
                      />
                    </div>)
                    : null}
                </div>
            </Card>
        )
    }
}

export default CarouselCard;

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@rmwc/typography';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { CardPrimaryAction } from '@rmwc/card';

import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

import './Carousel.css';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);



const styles = theme => ({
  // root: {
  //   maxWidth: 400,
  //   flexGrow: 1,
  // },
  header: {
    display: 'flex',
    alignItems: 'center',
    //height: 50,
    paddingLeft: theme.spacing.unit * 4,
    backgroundColor: theme.palette.background.default,
  },
  img: {
    maxHeight: 255,
    display: 'block',
    margin: 'auto',
    overflow: 'hidden'
  },
});

class SwipeableTextMobileStepper extends React.Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1,
    }));
  };

  handleStepChange = activeStep => {
    this.setState({ activeStep });
  };

  render() {

    const { classes, theme, pets = [] } = this.props;
    const { activeStep } = this.state;
    const maxSteps = pets.length;

    return (
      <div className={classes.root} style={{height: 350}}>
        <CardPrimaryAction>
          <AutoPlaySwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={activeStep}
            onChangeIndex={this.handleStepChange}
            enableMouseEvents
          >
            {pets.map((pet, index) => (
              <div key={pet.name}>
                {Math.abs(activeStep - index) <= 2 ? (
                  <div style={{textAlign: 'center'}}>
                    {pet.images[0] ?
                      (<img className={classes.img} src={pet.images[0].url} alt={pet.images[0].description} />)
                      : (<img className={classes.img} src={'http://petmedmd.com/images/dog-profile.png'} alt={'default image'} />)
                    }
                    <div style={{position: 'absolute', display: 'inline-block', maxWidth: 'auto', height: 'auto', backgroundColor: 'lightgray', borderTopRightRadius: 10, borderTopLeftRadius: 10, padding: "0 10px", opacity: 0.75, bottom: '0px', marginTop: "10px", transform: 'translate(-50%, 0%)'}} >
                      <h2 style={{margin: "5px 10px"}}>{pets[activeStep].name}</h2>
                    </div>
                  </div>
                ) : null}
              </div>
            ))}
          </AutoPlaySwipeableViews>
        </CardPrimaryAction>
        <MobileStepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          className={classes.mobileStepper}
          nextButton={
            <Button size="small" onClick={this.handleNext} disabled={activeStep === maxSteps - 1}>
              Next
              {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          }
          backButton={
            <Button size="small" onClick={this.handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
              Back
            </Button>
          }
        />
      </div>
    );
  }
}

SwipeableTextMobileStepper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  pets: PropTypes.array.isRequired,
};

export default withStyles(styles, { withTheme: true })(SwipeableTextMobileStepper);

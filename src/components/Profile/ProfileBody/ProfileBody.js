import React, {Component} from 'react';
import './ProfileBody.css';
import logo from '../../../resources/profileImage.jpg';
import CarouselCard from '../CarouselCard/CarouselCard';

class ProfileBody extends Component {
  render () {
    return (
      <div id='body-container'>
        <img src={logo} className="App-logo" id='profile-picture' alt="logo" />
        <div id='body-card'>
          {/*<div id="name-card"*/}

          <CarouselCard />


          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna et pharetra pharetra massa massa ultricies mi quis hendrerit. Ornare suspendisse sed nisi lacus sed viverra tellus. Ut enim blandit volutpat maecenas. Vel eros donec ac odio tempor orci dapibus ultrices. Tellus integer feugiat scelerisque varius morbi. Sit amet justo donec enim diam vulputate ut pharetra. Mattis enim ut tellus elementum sagittis vitae et. Pellentesque nec nam aliquam sem et tortor consequat id porta. Quisque egestas diam in arcu cursus euismod quis viverra. Odio euismod lacinia at quis risus sed vulputate odio ut. Sed pulvinar proin gravida hendrerit.

            Tellus rutrum tellus pellentesque eu. Dolor sed viverra ipsum nunc aliquet. Felis imperdiet proin fermentum leo vel orci porta. Sodales neque sodales ut etiam. Lectus proin nibh nisl condimentum id venenatis a condimentum. Lectus mauris ultrices eros in cursus turpis massa. Fermentum leo vel orci porta. Amet venenatis urna cursus eget nunc scelerisque viverra mauris. Feugiat nibh sed pulvinar proin gravida hendrerit lectus. Libero id faucibus nisl tincidunt eget nullam. Amet porttitor eget dolor morbi non arcu risus quis varius. In egestas erat imperdiet sed.

            Turpis egestas maecenas pharetra convallis posuere morbi. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Lacus viverra vitae congue eu consequat ac felis. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Consequat id porta nibh venenatis cras sed. Bibendum arcu vitae elementum curabitur vitae nunc sed. Etiam tempor orci eu lobortis. Libero nunc consequat interdum varius sit amet mattis. Consequat semper viverra nam libero justo laoreet sit amet. Sit amet tellus cras adipiscing.

            Consectetur libero id faucibus nisl. Amet purus gravida quis blandit turpis cursus in hac. Quam vulputate dignissim suspendisse in est. Nam aliquam sem et tortor consequat. Suspendisse in est ante in nibh mauris cursus mattis. Viverra vitae congue eu consequat ac felis. Leo urna molestie at elementum eu. Varius vel pharetra vel turpis nunc eget lorem. Quis varius quam quisque id diam. Vel pharetra vel turpis nunc eget. Arcu cursus euismod quis viverra nibh cras pulvinar mattis nunc. Morbi tristique senectus et netus. Magna sit amet purus gravida quis blandit.

            Laoreet non curabitur gravida arcu ac. In nibh mauris cursus mattis molestie a iaculis at erat. Dolor morbi non arcu risus quis varius quam quisque id. Iaculis eu non diam phasellus vestibulum lorem. Non blandit massa enim nec dui nunc mattis. At elementum eu facilisis sed odio morbi quis commodo odio. Ut lectus arcu bibendum at varius vel. Et ultrices neque ornare aenean euismod elementum. Mattis molestie a iaculis at erat. Augue interdum velit euismod in.

            Eros donec ac odio tempor orci. Pharetra massa massa ultricies mi quis. Odio facilisis mauris sit amet massa vitae. Tincidunt lobortis feugiat vivamus at augue eget arcu. Vulputate ut pharetra sit amet aliquam id diam. Aliquet nec ullamcorper sit amet risus nullam. Et netus et malesuada fames ac turpis egestas sed. Nam at lectus urna duis convallis convallis tellus. Viverra mauris in aliquam sem fringilla ut morbi. Aliquet enim tortor at auctor urna. Odio morbi quis commodo odio aenean sed. Sapien et ligula ullamcorper malesuada. Sit amet nisl suscipit adipiscing bibendum. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus.

            Bibendum at varius vel pharetra vel turpis nunc eget. Pharetra vel turpis nunc eget. Convallis tellus id interdum velit laoreet id donec. Dis parturient montes nascetur ridiculus mus mauris. Nisi lacus sed viverra tellus in hac habitasse platea. Risus at ultrices mi tempus imperdiet nulla. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit. Tellus integer feugiat scelerisque varius morbi enim. Cras pulvinar mattis nunc sed blandit. Malesuada proin libero nunc consequat. Magna eget est lorem ipsum dolor sit amet consectetur adipiscing. Non quam lacus suspendisse faucibus. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque. Nulla facilisi nullam vehicula ipsum a arcu cursus vitae.

            Nisi est sit amet facilisis magna etiam tempor orci. Egestas pretium aenean pharetra magna ac. Varius duis at consectetur lorem donec massa sapien. Arcu non sodales neque sodales ut etiam sit. Amet consectetur adipiscing elit ut aliquam purus sit amet. Risus nullam eget felis eget nunc lobortis mattis. Dui ut ornare lectus sit amet est placerat in egestas. Lobortis elementum nibh tellus molestie nunc non blandit. Lectus magna fringilla urna porttitor rhoncus dolor purus non enim. Malesuada fames ac turpis egestas. Sem nulla pharetra diam sit amet. Vitae suscipit tellus mauris a diam. Viverra justo nec ultrices dui sapien eget. Gravida arcu ac tortor dignissim convallis aenean. Massa tempor nec feugiat nisl pretium fusce id velit ut. Sed elementum tempus egestas sed. Pellentesque massa placerat duis ultricies lacus sed turpis tincidunt id. Magnis dis parturient montes nascetur ridiculus mus mauris vitae.

            Lectus arcu bibendum at varius vel pharetra vel turpis nunc. Id porta nibh venenatis cras. Nam libero justo laoreet sit amet cursus sit. Amet facilisis magna etiam tempor orci eu lobortis. Senectus et netus et malesuada. Tempus quam pellentesque nec nam aliquam sem. Non diam phasellus vestibulum lorem sed risus ultricies tristique nulla. Cras tincidunt lobortis feugiat vivamus at. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Felis donec et odio pellentesque diam volutpat commodo sed. Urna porttitor rhoncus dolor purus non. Morbi quis commodo odio aenean sed adipiscing diam. Aliquet lectus proin nibh nisl. Aenean sed adipiscing diam donec adipiscing tristique risus nec feugiat.

            Ipsum dolor sit amet consectetur adipiscing elit duis tristique. Diam sollicitudin tempor id eu. Magna etiam tempor orci eu lobortis elementum nibh tellus. In metus vulputate eu scelerisque. Quam viverra orci sagittis eu volutpat odio. Auctor neque vitae tempus quam pellentesque. Vitae suscipit tellus mauris a diam maecenas. Enim ut tellus elementum sagittis vitae et leo duis ut. Sed tempus urna et pharetra pharetra massa. Blandit volutpat maecenas volutpat blandit aliquam etiam. Pulvinar etiam non quam lacus suspendisse faucibus. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit eget gravida. Diam quam nulla porttitor massa id neque aliquam vestibulum morbi. Eu nisl nunc mi ipsum faucibus vitae. Scelerisque varius morbi enim nunc.

            Imperdiet nulla malesuada pellentesque elit eget gravida cum sociis. Pellentesque id nibh tortor id aliquet. Feugiat pretium nibh ipsum consequat. Urna duis convallis convallis tellus id interdum. Amet risus nullam eget felis eget nunc lobortis. Dis parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Lacus suspendisse faucibus interdum posuere lorem. Ultricies mi eget mauris pharetra et ultrices neque ornare aenean. Parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Commodo ullamcorper a lacus vestibulum. Tellus cras adipiscing enim eu turpis.

            Turpis tincidunt id aliquet risus feugiat in ante metus dictum. Interdum consectetur libero id faucibus nisl. Viverra accumsan in nisl nisi scelerisque eu. Volutpat sed cras ornare arcu dui. Dictum at tempor commodo ullamcorper a lacus. Id aliquet risus feugiat in ante metus dictum. Pellentesque eu tincidunt tortor aliquam. Vel eros donec ac odio tempor. Tincidunt id aliquet risus feugiat in ante metus dictum. Quis eleifend quam adipiscing vitae proin. Turpis massa sed elementum tempus egestas sed sed.
          </p>

        </div>
      </div>
    )
  }
}

export default ProfileBody

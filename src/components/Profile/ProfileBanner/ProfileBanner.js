import React, {Component} from 'react';
import './ProfileBanner.css';
import banner from '../../../resources/banner.jpg';

class ProfileBanner extends Component {
  render () {
    return (
      <div id='banner-container'>
        <img src={banner} ale='banner' />
      </div>
    )
  }
}

export default ProfileBanner

import React, {Component} from 'react';
import ProfileBody from './ProfileBody/ProfileBody';
import ProfileBanner from './ProfileBanner/ProfileBanner';

class Profile extends Component {
  render () {
    return (
      <div id='profile'>
        <ProfileBanner />
        <ProfileBody />
      </div>
    )
  }
}

export default Profile

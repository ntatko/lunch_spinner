import React, { Component } from 'react';
import logo from '../../resources/logo.svg';
import './App.css';

import Navbar from '../common/Navbar/Navbar';
import Profile from '../Profile/Profile';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Navbar />
        </header>
        <Profile />
      </div>
    );
  }
}

export default App;

import React, {Component} from 'react';
import './Navbar.css';
import AppBar from '@material-ui/core/AppBar';
import logo from '../../../resources/lunch.png';

class Navbar extends Component {
  render () {
    return (
      <div id='navbar-container'>
        <AppBar color='primary'>
          <div>
            <img className='navbar-image' id="navbar-logo" src={logo} alt="lunch?" />
          </div>
        </AppBar>
      </div>
    )
  }
}

export default Navbar
